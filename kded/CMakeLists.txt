set(dbus_interface_name org.kde.kbackup.xml)
set(kded_kbackup_PART_SRCS kded_kbackup.cpp)

add_library(kded_kbackup MODULE ${kded_kbackup_PART_SRCS})
set_target_properties(kded_kbackup PROPERTIES OUTPUT_NAME kded_kbackup)

target_link_libraries(kded_kbackup
   KF5::DBusAddons
   KF5::Plasma
)

kcoreaddons_desktop_to_json(kded_kbackup kded_kbackup.desktop)
install(TARGETS kded_kbackup DESTINATION ${PLUGIN_INSTALL_DIR}/kf5/kded)
