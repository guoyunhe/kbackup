// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2020 Guo Yunhe <i@guoyunhe.me>
// SPDX-FileCopyrightText: 2020 Harald Sitter <sitter@kde.org>

#include "kded_kbackup.h"

#include <KPluginFactory>

K_PLUGIN_CLASS_WITH_JSON(KBackupModule, "kded_kbackup.json")

KBackupModule::KBackupModule(QObject* parent, const QList<QVariant>&)
    : KDEDModule(parent)
    , config(KSharedConfig::openConfig(QStringLiteral("kscreenlockerrc"), KConfig::CascadeConfig))
    , configWatcher(KConfigWatcher::create(config))
{
    connect(configWatcher.data(), &KConfigWatcher::configChanged,
            this, &KBackupModule::configChanged);
    configChanged();
}

KBackupModule::~KBackupModule()
{
    delete consumer;
}

void KBackupModule::configChanged()
{
    if (engine) {
        engine->disconnectSource(previousSource, this);
    }

    previousSource = getSource();
    if (!previousSource.isEmpty()) {
        if (!consumer) {
            consumer = new Plasma::DataEngineConsumer;
            engine = consumer->dataEngine(QStringLiteral("kbackup"));
            engine->connectSource(previousSource, this); // trigger caching, no need to handle data
        }

        engine->connectSource(previousSource, this);
    }
}

QString KBackupModule::getSource()
{
    KConfigGroup greeterGroup = config->group(QStringLiteral("Greeter"));
    QString plugin = greeterGroup.readEntry(QStringLiteral("WallpaperPlugin"), QString());
    if (plugin != QStringLiteral("org.kde.kbackup")) {
        return QStringLiteral("");
    }

    KConfigGroup kbackupGroup = greeterGroup
                               .group(QStringLiteral("Wallpaper"))
                               .group(QStringLiteral("org.kde.kbackup"))
                               .group(QStringLiteral("General"));
    QString provider = kbackupGroup.readEntry(QStringLiteral("Provider"), QString());
    if (provider == QStringLiteral("unsplash")) {
        QString category = kbackupGroup.readEntry(QStringLiteral("Category"), QString());
        return provider + QStringLiteral(":") + category;
    } else {
        return provider;
    }
}

#include "kded_kbackup.moc"

